# Nightingaleish Theme

Nightingaleish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-nightingaleish_code.png](./images/sjsepan-nightingaleish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-nightingaleish_codium.png](./images/sjsepan-nightingaleish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-nightingaleish_codedev.png](./images/sjsepan-nightingaleish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-nightingaleish_ads.png](./images/sjsepan-nightingaleish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-nightingaleish_theia.png](./images/sjsepan-nightingaleish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-nightingaleish_positron.png](./images/sjsepan-nightingaleish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/13/2025
