# Nightingaleish Color Theme - Change Log

## [0.1.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.3]

- update readme and screenshot

## [0.1.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- define window border in custom mode
- fix menu FG/BG

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- buttons border, FG/BG
- fix manifest repo links
- retain v0.0.5 for those that prefer earlier style

## [0.0.5]

- Dim input placeholder FG, show inactive border

## [0.0.4]

- Fix Badges FG / BG
- Fix editor split border
- Fix terminal border
- Fix status bar border
- fix line numbers
- fix tab hover BG
- use dimmer borders

## [0.0.3]

- Fix typo in extension,vsixmanifest

## [0.0.2]

- Fix typo in package.json

## [0.0.1]

- Initial release
